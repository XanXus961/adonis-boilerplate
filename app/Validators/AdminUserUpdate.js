'use strict'

const Base = require('./Base')

class AdminUserUpdate extends Base {
  get rules() {
    const id = this.ctx.params.id
    return {
      // validation rules
      id: 'required|integer|exists:users,id',
      firstname: 'required|string|min:2',
      lastname: 'required|string|min:2',
      email: `required|email|unique:users,email,id,${id}`,
      is_active: 'required|boolean',
      is_admin: 'required|boolean',
      password: 'string|min:8',
      password_confirmation: 'required_if:password|string|same:password'
    }
  }
}

module.exports = AdminUserUpdate
