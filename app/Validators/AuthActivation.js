'use strict'

const crypto = require('crypto')
const Base = require('./Base')

class AuthActivation extends Base {
  get rules() {
    const activation_token = crypto
      .createHash('sha256')
      .update(this.ctx.request.all().email)
      .digest('hex')

    return {
      // validation rules
      email: 'required|email|exists:users,email',
      token: `required|string|equals:${activation_token}`
    }
  }

  get validateAll() {
    return true
  }
}

module.exports = AuthActivation
