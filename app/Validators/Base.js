class Base {
  get validateAll() {
    return true
  }

  async fails(errorMessages) {
    return this.ctx.response.badRequest(errorMessages)
  }

  get data() {
    const requestBody = this.ctx.request.all()
    return Object.assign({}, requestBody, this.ctx.params)
  }
}

module.exports = Base
