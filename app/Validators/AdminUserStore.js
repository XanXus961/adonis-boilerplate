'use strict'

const Base = require('./Base')

class AdminUserStore extends Base {
  get rules() {
    return {
      // validation rules
      firstname: 'required|string|min:2',
      lastname: 'required|string|min:2',
      email: 'required|email|unique:users,email',
      is_active: 'required|boolean',
      is_admin: 'required|boolean',
      password: 'required|string|min:8',
      password_confirmation: 'required|string|same:password'
    }
  }
}

module.exports = AdminUserStore
