'use strict'

const Base = require('./Base')

class AdminUserDestroy extends Base {
  get rules() {
    return {
      // validation rules
      id: 'required|integer|exists:users,id'
    }
  }
}

module.exports = AdminUserDestroy
