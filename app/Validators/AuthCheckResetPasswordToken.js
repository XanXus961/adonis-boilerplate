'use strict'

const Base = require('./Base')

class AuthCheckResetPasswordToken extends Base {
  get rules() {
    return {
      // validation rules
      token: 'required|string|exists:users,reset_token'
    }
  }

  get validateAll() {
    return true
  }
}

module.exports = AuthCheckResetPasswordToken
