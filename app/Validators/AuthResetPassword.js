'use strict'

const Base = require('./Base')

class AuthResetPassword extends Base {
  get rules() {
    return {
      // validation rules
      token: 'required|string|exists:users,reset_token',
      password: 'required|string|min:8',
      password_confirmation: 'required|string|same:password'
    }
  }

  get validateAll() {
    return true
  }
}

module.exports = AuthResetPassword
