'use strict'
const Base = require('./Base')

class AuthSignUp extends Base {
  get rules() {
    return {
      // validation rules
      firstname: 'required|string|min:2',
      lastname: 'required|string|min:2',
      email: 'required|email|unique:users,email',
      password: 'required|string|min:8',
      password_confirmation: 'required|string|same:password'
    }
  }
}

module.exports = AuthSignUp
