'use strict'

const Base = require('./Base')

class AuthForgotPassword extends Base {
  get rules() {
    return {
      // validation rules
      email: 'required|email|exists:users,email'
    }
  }

  get validateAll() {
    return true
  }
}

module.exports = AuthForgotPassword
