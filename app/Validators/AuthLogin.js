'use strict'

const Base = require('./Base')

class AuthLogin extends Base {
  get rules() {
    return {
      // validation rules
      email: 'required|email|exists:users,email',
      password: 'required|string'
    }
  }

  get validateAll() {
    return true
  }
}

module.exports = AuthLogin
