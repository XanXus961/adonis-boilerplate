'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

class Admin {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle({ request, auth, response }, next) {
    const user = await auth.getUser()

    if (!user.is_admin) {
      return response.forbidden({
        message: 'Access restricted'
      })
    } else {
      await next()
    }
  }
}

module.exports = Admin
