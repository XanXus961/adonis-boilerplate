'use strict'
const User = use('App/Models/User')
const Mail = use('Mail')
const Logger = use('Logger')
const Config = use('Config')
const uuid = use('uuid/v1')

const crypto = require('crypto')

class AuthController {
  /**
   * @swagger
   * /api/signup:
   *   post:
   *     description: Signup to the application
   *     produces:
   *        - application/json
   *     requestBody:
   *      content:
   *        application/json:
   *          schema:
   *            type: object
   *            properties:
   *              firstname:
   *                type: string
   *              lastname:
   *                type: string
   *              email:
   *                type: string
   *              password:
   *                type: string
   *              password_confirmation:
   *                type: string
   *            required:
   *              - firstname
   *              - lastname
   *              - email
   *              - password
   *              - password_confirmation
   *     responses:
   *       200:
   *         description: Returns the registred user
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *       400:
   *         description: Invalid input
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   */
  async signup({ request, response }) {
    // get the user data from request
    const { firstname, lastname, email, password } = request.all()
    // create user
    const user = await User.create({ firstname, lastname, email, password })

    const activation_token = crypto
      .createHash('sha256')
      .update(email)
      .digest('hex')

    Mail.send('emails.activation', { user, activation_token }, (message) => {
      message
        .from(Config.get('mail.from'))
        .to(email)
        .subject('Confirm email address')
    })
      .then(() => Logger.info('Confirmation email sent to: ' + email))
      .catch((err) => {
        Logger.crit('Confirmation email not sent to: ' + email, err)
      })

    // return response with newly created user
    return response.ok({
      user: {
        firstname: user.firstname,
        lastname: user.lastname,
        email: user.email
      }
    })
  }

  /**
   * @swagger
   * /api/activation:
   *   get:
   *     description: Activate your account
   *     produces:
   *        - application/json
   *     parameters:
   *       - name: email
   *         description: Email of the user
   *         in: query
   *         required: true
   *         type: string
   *       - name: token
   *         description: Received token
   *         in: query
   *         required: true
   *         type: string
   *     responses:
   *       200:
   *         description: Returns the registred user
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *       400:
   *         description: Invalid input
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *       422:
   *         description: Usera already active
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   */
  async activation({ request, response }) {
    // get the user data from request
    const { email } = request.all()

    // fetch user
    const user = await User.findBy('email', email)

    if (!user.is_active) {
      // activate user
      user.is_active = true
      await user.save()
      Logger.info('Email ' + user.email + ' confirmed')
      return response.ok({
        user: {
          firstname: user.firstname,
          lastname: user.lastname,
          email: user.email
        }
      })
    } else {
      // throw an error if user already activated
      return response.unprocessableEntity({
        message: 'User already active'
      })
    }
  }

  /**
   * @swagger
   * /api/resendActivationEmail:
   *   get:
   *     description: Resend activation email
   *     produces:
   *        - application/json
   *     parameters:
   *       - name: email
   *         description: Email of the user
   *         in: query
   *         required: true
   *         type: string
   *     responses:
   *       200:
   *         description: Email sent
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *       400:
   *         description: Invalid input
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *       422:
   *         description: Usera already active
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   */
  async resendActivationEmail({ request, response }) {
    // get the user data from request
    const { email } = request.all()

    // fetch user
    const user = await User.findBy('email', email)

    if (!user.is_active) {
      const activation_token = crypto
        .createHash('sha256')
        .update(email)
        .digest('hex')

      Mail.send('emails.activation', { user, activation_token }, (message) => {
        message
          .from(Config.get('mail.from'))
          .to(email)
          .subject('Confirm email address')
      })
        .then(() => Logger.info('Confirmation email sent to: ' + email))
        .catch((err) => {
          Logger.crit('Confirmation email not sent to: ' + email, err)
        })

      return response.ok({
        message: 'Email sent'
      })
    } else {
      // throw an error if user already activated
      return response.unprocessableEntity({
        message: 'User already active'
      })
    }
  }

  /**
   * @swagger
   * /api/login:
   *   post:
   *     description: Resend activation email
   *     produces:
   *        - application/json
   *     requestBody:
   *      content:
   *        application/json:
   *          schema:
   *            type: object
   *            properties:
   *              email:
   *                type: string
   *              password:
   *                type: string
   *            required:
   *              - email
   *              - password
   *     responses:
   *       200:
   *         description: Returns tokens
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *       400:
   *         description: Invalid input
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *       401:
   *         description: Invalid credentials
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *       403:
   *         description: Email not confirmed
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   */
  async login({ request, auth, response }) {
    // get the user data from request
    const { email, password } = request.all()

    try {
      // attemp login
      const token = await auth.withRefreshToken().attempt(email, password)
      // fetch user
      const user = await User.findBy('email', email)
      // check if user is active
      if (!user.is_active) {
        return response.forbidden({
          message: 'Email not confirmed'
        })
      }

      // return response with newly created token
      return response.ok({ token })
    } catch (e) {
      return response.unauthorized({
        message: 'Wrong credentials'
      })
    }
  }

  /**
   * @swagger
   * /api/refreshToken:
   *   post:
   *     description: Get new token from refresh token
   *     produces:
   *        - application/json
   *     requestBody:
   *      content:
   *        application/json:
   *          schema:
   *            type: object
   *            properties:
   *              refresh_token:
   *                type: string
   *            required:
   *              - refresh_token
   *     responses:
   *       200:
   *         description: Returns tokens
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *       400:
   *         description: Invalid input
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *       401:
   *         description: Invalid refresh token
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   */
  async refreshToken({ request, auth, response }) {
    // get the user data from request
    const { refresh_token } = request.all()

    try {
      // generate new token
      const token = await auth
        .newRefreshToken()
        .generateForRefreshToken(refresh_token)

      // return response with newly created token
      return response.ok({ token })
    } catch (e) {
      return response.unauthorized({
        message: 'Invalid refresh token'
      })
    }
  }

  /**
   * @swagger
   * /api/forgotPassword:
   *   post:
   *     description: Send forgot password email
   *     produces:
   *        - application/json
   *     requestBody:
   *      content:
   *        application/json:
   *          schema:
   *            type: object
   *            properties:
   *              email:
   *                type: string
   *            required:
   *              - email
   *     responses:
   *       200:
   *         description: Reset password email has been sent
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *       400:
   *         description: Invalid input
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   */
  async forgotPassword({ request, response }) {
    // get the user data from request
    const { email } = request.all()

    // fetch user
    const user = await User.findBy('email', email)

    // add reset token to user
    user.reset_token = uuid()
    user.reset_token_expires_at = Date.now() + 3600000 // 1 hour
    await user.save()

    // send email
    Mail.send('emails.forgotPassword', { user }, (message) => {
      message.from(Config.get('mail.from')).to(email).subject('Reset Password')
    })
      .then(() => Logger.info('Reset password email sent for: ' + user.email))
      .catch((err) => {
        Logger.error('Reset password email not sent for: ' + user.email, err)
      })

    return response.ok({ message: 'Reset password email has been sent' })
  }

  /**
   * @swagger
   * /api/checkResetPasswordToken:
   *   get:
   *     description: Check forgot password token validity
   *     produces:
   *        - application/json
   *     parameters:
   *       - name: token
   *         description: Received token
   *         in: query
   *         required: true
   *         type: string
   *     responses:
   *       200:
   *         description: Valid token
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *       400:
   *         description: Invalid input
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *       401:
   *         description: Expired token
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   */
  async checkResetPasswordToken({ request, response }) {
    // get the user data from request
    const { token } = request.all()

    // fetch user
    const user = await User.findBy('reset_token', token)

    // check if token is still valid
    if (
      Date.parse(user.reset_token_expires_at) - Date.now() <=
      1000 /* 1 second */
    ) {
      return response.unauthorized({
        message: 'Expired token'
      })
    }

    return response.ok({ message: 'Valid token' })
  }

  /**
   * @swagger
   * /api/resetPassword:
   *   post:
   *     description: Reset your password
   *     produces:
   *        - application/json
   *     requestBody:
   *      content:
   *        application/json:
   *          schema:
   *            type: object
   *            properties:
   *              token:
   *                type: string
   *              password:
   *                type: string
   *              password_confirmation:
   *                type: string
   *            required:
   *              - token
   *              - password
   *              - password_confirmation
   *     responses:
   *       200:
   *         description: Password has been changed
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *       400:
   *         description: Invalid input
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *       401:
   *         description: Expired token
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   */
  async resetPassword({ request, response }) {
    // get the user data from request
    const { token, password } = request.all()

    // fetch user
    const user = await User.findBy('reset_token', token)

    // check if token is still valid
    if (
      Date.parse(user.reset_token_expires_at) - Date.now() <=
      1000 /* 1 second */
    ) {
      return response.unauthorized({
        message: 'Expired token'
      })
    }

    // add new password to user
    user.password = password
    user.reset_token = null
    user.reset_token_expires_at = null
    await user.save()

    return response.ok({ message: 'Password has been changed' })
  }
}

module.exports = AuthController
