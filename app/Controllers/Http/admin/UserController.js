'use strict'

const User = use('App/Models/User')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with users
 */
class UserController {
  /**
   * @swagger
   * /api/admin/users:
   *   get:
   *     description: Show a list of all users
   *     security:
   *          - bearerAuth: []
   *     produces:
   *        - application/json
   *     responses:
   *       200:
   *         description: Returns an array of users
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *       401:
   *         description: Unauthorized
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *       403:
   *         description: Access restricted
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   */
  /**
   * Show a list of all users.
   * GET users
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response }) {
    const users = await User.all()
    const jsonUsers = users.toJSON()
    const mappedUsers = jsonUsers.map((user) => {
      delete user.password
      return user
    })
    return response.ok(mappedUsers)
  }

  /**
   * @swagger
   * /api/admin/users:
   *   post:
   *     description: Create a new user
   *     security:
   *          - bearerAuth: []
   *     produces:
   *        - application/json
   *     requestBody:
   *      content:
   *        application/json:
   *          schema:
   *            type: object
   *            properties:
   *              firstname:
   *                type: string
   *              lastname:
   *                type: string
   *              email:
   *                type: string
   *              password:
   *                type: string
   *              password_confirmation:
   *                type: string
   *              is_active:
   *                type: boolean
   *              is_admin:
   *                type: boolean
   *            required:
   *              - firstname
   *              - lastname
   *              - email
   *              - password
   *              - password_confirmation
   *              - is_active
   *              - is_admin
   *     responses:
   *       200:
   *         description: Returns the created user
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *       400:
   *         description: Invalid input
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *       401:
   *         description: Unauthorized
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *       403:
   *         description: Access restricted
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   */
  /**
   * Create/save a new user.
   * POST users
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    // get the user data from request
    const {
      firstname,
      lastname,
      email,
      password,
      is_active,
      is_admin
    } = request.all()
    // create user
    const user = await User.create({
      firstname,
      lastname,
      email,
      password,
      is_active,
      is_admin
    })

    const jsonUser = user.toJSON()
    delete jsonUser.password

    // return response with newly created user
    return response.ok(jsonUser)
  }

  /**
   * @swagger
   * /api/admin/users/{id}:
   *   put:
   *     description: Update user details
   *     security:
   *          - bearerAuth: []
   *     produces:
   *        - application/json
   *     requestBody:
   *      content:
   *        application/json:
   *          schema:
   *            type: object
   *            properties:
   *              firstname:
   *                type: string
   *              lastname:
   *                type: string
   *              email:
   *                type: string
   *              password:
   *                type: string
   *              password_confirmation:
   *                type: string
   *              is_active:
   *                type: boolean
   *              is_admin:
   *                type: boolean
   *            required:
   *              - firstname
   *              - lastname
   *              - email
   *              - password
   *              - password_confirmation
   *              - is_active
   *              - is_admin
   *     parameters:
   *       - name: id
   *         description: ID of the user
   *         in: path
   *         required: true
   *         type: integer
   *     responses:
   *       200:
   *         description: Returns the updated user
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *       400:
   *         description: Invalid input
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *       401:
   *         description: Unauthorized
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *       403:
   *         description: Access restricted
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   */
  /**
   * Update user details.
   * PUT or PATCH users/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ request, params, response }) {
    const editedUser = request.all()
    delete editedUser.password_confirmation
    // fetch user
    const user = await User.find(params.id)
    user.merge(editedUser)
    await user.save()

    const jsonUser = user.toJSON()
    delete jsonUser.password

    // return response with newly created user
    return response.ok(jsonUser)
  }

  /**
   * @swagger
   * /api/admin/users/{id}:
   *   delete:
   *     description: Delete a user with id
   *     security:
   *          - bearerAuth: []
   *     produces:
   *        - application/json
   *     parameters:
   *       - name: id
   *         description: ID of the user
   *         in: path
   *         required: true
   *         type: integer
   *     responses:
   *       200:
   *         description: Returns the deleted user
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *       400:
   *         description: Invalid input
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *       401:
   *         description: Unauthorized
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *       403:
   *         description: Access restricted
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   */
  /**
   * Delete a user with id.
   * DELETE users/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ request, params, response }) {
    const user = await User.find(params.id)
    await user.delete()

    const jsonUser = user.toJSON()
    delete jsonUser.password

    // return response with newly created user
    return response.ok(jsonUser)
  }
}

module.exports = UserController
