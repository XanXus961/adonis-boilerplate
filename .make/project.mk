.PHONY: help
help::
	@printf "\n"
	@printf "\033[1;33m Project:\033[0m\n"
	@printf "\033[1;32m   install              \033[0;39m Install project.\033[0m\n"
	@printf "\033[1;32m   run                  \033[0;39m Run containers.\033[0m\n"
	@printf "\033[1;32m   stop                 \033[0;39m Stop containers.\033[0m\n"
	@printf "\033[1;32m   clean                \033[0;39m Stop containers, clean docker & project.\033[0m\n"

.PHONY: install
install: --copy-env --setup-network --build-container --install-dependencies --run-migation --generate-key 

.PHONY: run
run:
	$(DOCKER_COMPOSE) up -d

.PHONY: stop
stop:
	$(DOCKER_COMPOSE) stop

.PHONY: clean
clean: --clean-docker --clean-project
