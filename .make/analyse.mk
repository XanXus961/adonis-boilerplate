.PHONY: help
help::
	@printf "\n"
	@printf "\033[1;33m Analyse:\033[0m\n"
	@printf "\033[1;32m   lint                 \033[0;39m Check lint.\033[0m\n"
	@printf "\033[1;32m   lint-fix             \033[0;39m Fix lint.\033[0m\n"
	@printf "\033[1;32m   test                 \033[0;39m Run test.\033[0m\n"
	
.PHONY: lint
lint:
	$(DOCKER_COMPOSE_RUN_NODE) yarn run lint

.PHONY: lint-fix
lint-fix:
	$(DOCKER_COMPOSE_RUN_NODE) yarn run lint --fix
	
.PHONY: test
test:
	$(DOCKER_COMPOSE_RUN_NODE) yarn run test
