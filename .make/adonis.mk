.PHONY: help
help::
	@printf "\n"
	@printf "\033[1;33m Adonis:\033[0m\n"
	@printf "\033[1;32m   seed-run             \033[0;39m Run adonis seed.\033[0m\n"
	@printf "\033[1;32m   migration-run        \033[0;39m Run adonis migration:run.\033[0m\n"
	@printf "\033[1;32m   migration-refresh    \033[0;39m Run adonis migration:refresh.\033[0m\n"
	@printf "\033[1;32m   migration-rollback   \033[0;39m Run adonis migration:rollback.\033[0m\n"

.PHONY: migration-run
migration-run:
	$(DOCKER_COMPOSE_RUN_NODE) adonis migration:run

.PHONY: migration-refresh
migration-refresh:
	$(DOCKER_COMPOSE_RUN_NODE) adonis migration:refresh

.PHONY: migration-rollback
migration-rollback:
	$(DOCKER_COMPOSE_RUN_NODE) adonis migration:rollback

.PHONY: seed-run
seed-run:
	$(DOCKER_COMPOSE_RUN_NODE) adonis seed