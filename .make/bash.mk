.PHONY: help
help::
	@printf "\n"
	@printf "\033[1;33m Bash:\033[0m\n"
	@printf "\033[1;32m   bash-node            \033[0;39m Run bash in node container.\033[0m\n"
	@printf "\033[1;32m   bash-mysql           \033[0;39m Run bash in mysql container.\033[0m\n"

.PHONY: bash-node
bash-node: run
	$(DOCKER_COMPOSE) exec adonis-node bash

.PHONY: bash-mysql
bash-mysql: run
	$(DOCKER_COMPOSE) exec adonis-mysql bash