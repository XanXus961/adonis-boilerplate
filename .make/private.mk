--copy-env:
	@echo "Copy env if not existing"
	@cp -n .env.example .env

--setup-network:
	@(docker network ls --format "{{.Name}}" --filter Name=^$(APP_NETWORK)$ | grep ^$(APP_NETWORK)$ || docker network create $(APP_NETWORK))

--build-container:
	$(DOCKER_COMPOSE) build

--install-dependencies:
	$(DOCKER_COMPOSE_RUN_NODE) yarn install

--clean-docker:
	$(DOCKER_COMPOSE) down --volumes --remove-orphans

--clean-project:
	@echo "Removing generated files & folders"
	@rm -rf node_modules .env

--run-migation:
	$(DOCKER_COMPOSE_RUN_NODE) adonis migration:run

--generate-key:
	$(DOCKER_COMPOSE_RUN_NODE) adonis key:generate