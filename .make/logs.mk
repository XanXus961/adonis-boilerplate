.PHONY: help
help::
	@printf "\n"
	@printf "\033[1;33m Logs:\033[0m\n"
	@printf "\033[1;32m   logs                 \033[0;39m Display all logs.\033[0m\n"
	@printf "\033[1;32m   logs-node            \033[0;39m Display node logs.\033[0m\n"
	@printf "\033[1;32m   logs-mysql           \033[0;39m Display mongodb logs.\033[0m\n"

.PHONY: logs
logs:
	$(DOCKER_COMPOSE) logs

.PHONY: logs-node
logs-node:
	$(DOCKER_COMPOSE) logs adonis-node

.PHONY: logs-mysql
logs-mysql:
	$(DOCKER_COMPOSE) logs adonis-mysql