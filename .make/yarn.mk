.PHONY: help
help::
	@printf "\n"
	@printf "\033[1;33m Yarn:\033[0m\n"
	@printf "\033[1;32m   yarn-install         \033[0;39m Run yarn install.\033[0m\n"

.PHONY: yarn-install
yarn-install:
	$(DOCKER_COMPOSE_RUN_NODE) yarn install