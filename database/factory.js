'use strict'

/*
|--------------------------------------------------------------------------
| Factory
|--------------------------------------------------------------------------
|
| Factories are used to define blueprints for database tables or Lucid
| models. Later you can use these blueprints to seed your database
| with dummy data.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Hash = use('Hash')

Factory.blueprint('App/Models/User', async (faker, i, data) => {
  return {
    firstname: faker.first({ nationality: 'fr' }),
    lastname: faker.last({ nationality: 'fr' }),
    email: data.email
      ? data.email
      : faker.email({ domain: 'content-machine.com' }),
    password: data.password ? data.password : faker.password(),
    is_active:
      typeof data.is_active === 'boolean'
        ? data.is_active
        : faker.bool({ likelihood: 30 }),
    is_admin: typeof data.is_admin === 'boolean' ? data.is_admin : false
  }
})
