'use strict'

/*
|--------------------------------------------------------------------------
| UserSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Database = use('Database')

class UserSeeder {
  async run() {
    await Database.raw('SET FOREIGN_KEY_CHECKS = 0;')
    await Database.truncate('users')
    await Database.raw('SET FOREIGN_KEY_CHECKS = 1;')

    await Factory.model('App/Models/User').create({
      email: 'admin@content-machine.com',
      password: 'Admin1234',
      is_active: true,
      is_admin: true
    })
    await Factory.model('App/Models/User').create({
      email: 'marouane@content-machine.com',
      password: 'Test1234',
      is_active: true
    })
    await Factory.model('App/Models/User').create({
      email: 'zouhair@content-machine.com',
      password: 'Test1234',
      is_active: true
    })
    await Factory.model('App/Models/User').createMany(5, {
      password: 'Test1234'
    })
  }
}

module.exports = UserSeeder
