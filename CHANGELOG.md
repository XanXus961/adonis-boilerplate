## [0.1.1] - 2020-10-29
### Added
- README, CHANGELOG, CONTRIBUTING and CODE_OF_CONDUCT files
### Bugfix
- CI tests

## [0.1.0] - 2020-09-30
### Added
- signup
- login
- reset password
- resfresh token
- email activation
- user CRUD
- dockerisation
- testing
- eslint
- pre-commit hook
- swagger
- cors
