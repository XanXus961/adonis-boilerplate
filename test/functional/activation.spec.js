'use strict'

const Mail = use('Mail')
const Factory = use('Factory')
const { test, trait, before, after } = use('Test/Suite')('Activation')

const ace = require('@adonisjs/ace')

const crypto = require('crypto')

before(async () => {
  await Factory.model('App/Models/User').create({
    email: 'activated@content-machine.com',
    password: 'Test1234',
    is_active: true
  })
  await Factory.model('App/Models/User').create({
    email: 'inactivated@content-machine.com',
    password: 'Test1234',
    is_active: false
  })
  Mail.fake()
})

after(async () => {
  await ace.call('migration:refresh', {}, { silent: true })
  Mail.restore()
})

trait('Test/ApiClient')

test('should not resend activation email to an activated user', async ({
  client,
  assert
}) => {
  // inactivated user data
  const activated_user = {
    email: 'activated@content-machine.com'
  }

  // make api request to login the user
  const response = await client
    .get('api/resendActivationEmail?email=' + activated_user.email)
    .end()

  // assert the status is 422
  response.assertStatus(422)

  assert.equal(response.body.message, 'User already active')
})

test('should not resend activation email to an inactivated user successfully if email is not present or is malformatted', async ({
  client,
  assert
}) => {
  // inactivated user data
  let inactivated_user = {}

  // make api request to login the user
  let response = await client.get('api/resendActivationEmail').end()

  // assert the status is 200
  response.assertStatus(400)

  assert.equal(response.body[0].field, 'email')
  assert.equal(response.body[0].validation, 'required')

  // inactivated user data
  inactivated_user = { email: 'inactivated@content-machine.' }

  // make api request to login the user
  response = await client
    .get('api/resendActivationEmail?email=' + inactivated_user.email)
    .send(inactivated_user)
    .end()

  // assert the status is 200
  response.assertStatus(400)

  assert.equal(response.body[0].field, 'email')
  assert.equal(response.body[0].validation, 'email')
})

test('should resend activation email to an inactivated user successfully', async ({
  client,
  assert
}) => {
  // inactivated user data
  const inactivated_user = {
    email: 'inactivated@content-machine.com'
  }

  // make api request to login the user
  const response = await client
    .get('api/resendActivationEmail?email=' + inactivated_user.email)
    .end()

  // assert the status is 200
  response.assertStatus(200)

  assert.equal(response.body.message, 'Email sent')

  setTimeout(() => {
    const recentEmail = Mail.pullRecent()
    assert.equal(
      recentEmail.message.to[0].address,
      'inactivated@content-machine.com'
    )
  }, 5000)
})

test('should not activate an activated', async ({ client, assert }) => {
  // activated user data
  const activated_user = {
    email: 'activated@content-machine.com',
    token: crypto
      .createHash('sha256')
      .update('activated@content-machine.com')
      .digest('hex')
  }

  // make api request to login the user
  const response = await client
    .get(
      'api/activation?email=' +
        activated_user.email +
        '&token=' +
        activated_user.token
    )
    .end()

  // assert the status is 422
  response.assertStatus(422)

  assert.equal(response.body.message, 'User already active')
})

test('should not activate an inactivated user successfully if one of the fields is not present', async ({
  client,
  assert
}) => {
  // inactivated user data
  let inactivated_user = {
    email: 'inactivated@content-machine.com'
  }

  // make api request to login the user
  let response = await client
    .get('api/activation?email=' + inactivated_user.email)
    .end()
  // assert the status is 400
  response.assertStatus(400)

  assert.equal(response.body[0].field, 'token')
  assert.equal(response.body[0].validation, 'required')

  // // inactivated user data
  // inactivated_user = {
  //   token: crypto
  //     .createHash('sha256')
  //     .update('inactivated@content-machine.com')
  //     .digest('hex')
  // }

  // console.log(inactivated_user)
  // // make api request to login the user
  // response = await client.get('api/activation?email=').send(inactivated_user).end()

  // // assert the status is 400
  // response.assertStatus(400)

  // assert.equal(response.body[0].field, 'email')
  // assert.equal(response.body[0].validation, 'required')
})

test('should not activate an inactivated user successfully if the email is malformatted', async ({
  client,
  assert
}) => {
  // inactivated user data
  const inactivated_user = {
    email: 'inactivated@content-machine.',
    token: crypto
      .createHash('sha256')
      .update('inactivated@content-machine.com')
      .digest('hex')
  }

  // make api request to login the user
  const response = await client
    .get(
      'api/activation?email=' +
        inactivated_user.email +
        '&token=' +
        inactivated_user.token
    )
    .end()

  // assert the status is 400
  response.assertStatus(400)

  assert.equal(response.body[0].field, 'email')
  assert.equal(response.body[0].validation, 'email')
})

test('should not activate an inactivated user successfully if the token is incorrect', async ({
  client,
  assert
}) => {
  // inactivated user data
  const inactivated_user = {
    email: 'inactivated@content-machine.com',
    token: crypto
      .createHash('sha256')
      .update('marouane@content-machine.com')
      .digest('hex')
  }

  // make api request to login the user
  const response = await client
    .get(
      'api/activation?email=' +
        inactivated_user.email +
        '&token=' +
        inactivated_user.token
    )
    .end()

  // assert the status is 400
  response.assertStatus(400)

  assert.equal(response.body[0].field, 'token')
  assert.equal(response.body[0].validation, 'equals')
})

test('should activate an inactivated successfully', async ({
  client,
  assert
}) => {
  // inactivated user data
  const inactivated_user = {
    email: 'inactivated@content-machine.com',
    token: crypto
      .createHash('sha256')
      .update('inactivated@content-machine.com')
      .digest('hex')
  }

  // make api request to login the user
  const response = await client
    .get(
      'api/activation?email=' +
        inactivated_user.email +
        '&token=' +
        inactivated_user.token
    )
    .end()

  // assert the status is 200
  response.assertStatus(200)
  // assert the user is in the response
  assert.isDefined(response.body.user)
  assert.equal(response.body.user.email, inactivated_user.email)
})
