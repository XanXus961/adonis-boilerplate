'use strict'

const User = use('App/Models/User')
const Factory = use('Factory')
const Mail = use('Mail')
const { test, trait, before, after } = use('Test/Suite')('Reset Password')

const ace = require('@adonisjs/ace')

before(async () => {
  await Factory.model('App/Models/User').create({
    email: 'user@content-machine.com',
    password: 'Test1234',
    is_active: true
  })
  Mail.fake()
})

after(async () => {
  await ace.call('migration:refresh', {}, { silent: true })
  Mail.restore()
})

trait('Test/ApiClient')

test('should send forgot password email successfully', async ({
  client,
  assert
}) => {
  // user data
  const user = {
    email: 'user@content-machine.com'
  }

  // make api request to login the user
  const response = await client.post('api/forgotPassword').send(user).end()

  // assert the status is 200
  response.assertStatus(200)
  assert.equal(response.body.message, 'Reset password email has been sent')

  setTimeout(() => {
    const recentEmail = Mail.pullRecent()
    assert.equal(recentEmail.message.to[0].address, 'user@content-machine.com')
  }, 5000)
})

test('should not send forgot password email successfully if email is not present', async ({
  client,
  assert
}) => {
  // user data
  const user = {}

  // make api request to login the user
  const response = await client.post('api/forgotPassword').send(user).end()

  // assert the status is 400
  response.assertStatus(400)

  assert.equal(response.body[0].field, 'email')
  assert.equal(response.body[0].validation, 'required')
})

test('should not send forgot password email successfully if email is malformatted', async ({
  client,
  assert
}) => {
  // user data
  const user = {
    email: 'user@content-machine.'
  }

  // make api request to login the user
  const response = await client.post('api/forgotPassword').send(user).end()

  // assert the status is 400
  response.assertStatus(400)

  assert.equal(response.body[0].field, 'email')
  assert.equal(response.body[0].validation, 'email')
})

test('should check reset token as valid', async ({ client, assert }) => {
  // fetch user
  const user = await User.findBy('email', 'user@content-machine.com')
  // user data
  const token = {
    token: user.reset_token
  }

  // make api request to login the user
  const response = await client
    .get('api/checkResetPasswordToken?token=' + token.token)
    .end()

  // assert the status is 200
  response.assertStatus(200)
  assert.equal(response.body.message, 'Valid token')
})

test('should check reset token as invalid', async ({ client, assert }) => {
  // user data
  const token = {
    token: '1234-5678-9012-3456-7890'
  }

  // make api request to login the user
  const response = await client
    .get('api/checkResetPasswordToken?token=' + token.token)
    .end()

  // assert the status is 400
  response.assertStatus(400)

  assert.equal(response.body[0].field, 'token')
  assert.equal(response.body[0].validation, 'exists')
})

test('should check reset token as expired', async ({ client, assert }) => {
  // fetch user
  const user = await User.findBy('email', 'user@content-machine.com')
  user.reset_token_expires_at = new Date(
    new Date(user.reset_token_expires_at) - 3600000
  ) // substruct one hour
  await user.save()

  // user data
  const token = {
    token: user.reset_token
  }

  // make api request to login the user
  const response = await client
    .get('api/checkResetPasswordToken?token=' + token.token)
    .end()

  // assert the status is 401
  response.assertStatus(401)

  assert.equal(response.body.message, 'Expired token')

  // restore reset password data
  await client
    .post('api/forgotPassword')
    .send({
      email: 'user@content-machine.com'
    })
    .end()
})

test('should not reset password if token is invalid', async ({
  client,
  assert
}) => {
  // user data
  const body = {
    token: '1234-5678-9012-3456-7890',
    password: '12345678',
    password_confirmation: '12345678'
  }

  // make api request to login the user
  const response = await client.post('api/resetPassword').send(body).end()

  // assert the status is 400
  response.assertStatus(400)

  assert.equal(response.body[0].field, 'token')
  assert.equal(response.body[0].validation, 'exists')
})

test('should not reset password if token is expired', async ({
  client,
  assert
}) => {
  // fetch user
  const user = await User.findBy('email', 'user@content-machine.com')
  user.reset_token_expires_at = new Date(
    new Date(user.reset_token_expires_at) - 3600000
  ) // substruct one hour
  await user.save()
  // user data
  const body = {
    token: user.reset_token,
    password: '12345678',
    password_confirmation: '12345678'
  }

  // make api request to login the user
  const response = await client.post('api/resetPassword').send(body).end()

  // assert the status is 401
  response.assertStatus(401)

  assert.equal(response.body.message, 'Expired token')

  // restore reset password data
  await client
    .post('api/forgotPassword')
    .send({
      email: 'user@content-machine.com'
    })
    .end()
})

test('should not reset password successfully if one of the fields is not present', async ({
  client,
  assert
}) => {
  // fetch user
  const user = await User.findBy('email', 'user@content-machine.com')
  // user data
  let body = {
    password: '12345678',
    password_confirmation: '12345678'
  }

  // make api request to login the user
  let response = await client.post('api/resetPassword').send(body).end()

  // assert the status is 200
  response.assertStatus(400)

  assert.equal(response.body[0].field, 'token')
  assert.equal(response.body[0].validation, 'required')

  // user data
  body = {
    token: user.reset_token,
    password_confirmation: '12345678'
  }

  // make api request to login the user
  response = await client.post('api/resetPassword').send(body).end()

  // assert the status is 200
  response.assertStatus(400)

  assert.equal(response.body[0].field, 'password')
  assert.equal(response.body[0].validation, 'required')

  // user data
  body = {
    token: user.reset_token,
    password: '12345678'
  }

  // make api request to login the user
  response = await client.post('api/resetPassword').send(body).end()

  // assert the status is 200
  response.assertStatus(400)

  assert.equal(response.body[0].field, 'password_confirmation')
  assert.equal(response.body[0].validation, 'required')
})

test('should not reset password successfully if password is less than 8 chars', async ({
  client,
  assert
}) => {
  // fetch user
  const user = await User.findBy('email', 'user@content-machine.com')
  // user data
  let body = {
    token: user.reset_token,
    password: '1234567',
    password_confirmation: '1234567'
  }

  // make api request to login the user
  let response = await client.post('api/resetPassword').send(body).end()

  // assert the status is 200
  response.assertStatus(400)

  assert.equal(response.body[0].field, 'password')
  assert.equal(response.body[0].validation, 'min')
})

test('should not reset password successfully if password_confirmation is different than password', async ({
  client,
  assert
}) => {
  // fetch user
  const user = await User.findBy('email', 'user@content-machine.com')
  // user data
  let body = {
    token: user.reset_token,
    password: '12345678',
    password_confirmation: '12345679'
  }

  // make api request to login the user
  let response = await client.post('api/resetPassword').send(body).end()

  // assert the status is 200
  response.assertStatus(400)

  assert.equal(response.body[0].field, 'password_confirmation')
  assert.equal(response.body[0].validation, 'same')
})

test('should reset password successfully', async ({ client, assert }) => {
  // fetch user
  const user = await User.findBy('email', 'user@content-machine.com')
  // user data
  const body = {
    token: user.reset_token,
    password: '12345678',
    password_confirmation: '12345678'
  }

  // make api request to login the user
  const response = await client.post('api/resetPassword').send(body).end()

  // assert the status is 200
  response.assertStatus(200)
  assert.equal(response.body.message, 'Password has been changed')
})
