'use strict'

const Mail = use('Mail')
const { test, trait, before, after } = use('Test/Suite')('Sign Up')

const ace = require('@adonisjs/ace')

before(async () => {
  Mail.fake()
})

after(async () => {
  await ace.call('migration:refresh', {}, { silent: true })
  Mail.restore()
})

trait('Test/ApiClient')

test('should signUp a user successfully', async ({ client, assert }) => {
  // new user data
  const new_user = {
    firstname: 'john',
    lastname: 'cena',
    email: 'john@content-machine.com',
    password: '12345678',
    password_confirmation: '12345678'
  }

  // make api request to signup the user
  const response = await client.post('api/signup').send(new_user).end()

  // assert the status is 200
  response.assertStatus(200)
  // assert the user is in the response
  assert.isDefined(response.body.user)
  // assert the returned user is equal to the sent one
  response.assertJSON({
    user: {
      firstname: new_user.firstname,
      lastname: new_user.lastname,
      email: new_user.email
    }
  })

  setTimeout(() => {
    const recentEmail = Mail.pullRecent()
    assert.equal(recentEmail.message.to[0].address, 'user@content-machine.com')
  }, 5000)
})

test('should not signUp a user successfully if one of the fields is not present', async ({
  client,
  assert
}) => {
  // new user data
  let new_user = {
    lastname: 'cena',
    email: 'cena@content-machine.com',
    password: '12345678',
    password_confirmation: '12345678'
  }

  // make api request to signup the user
  let response = await client.post('api/signup').send(new_user).end()

  // assert the status is 200
  response.assertStatus(400)

  assert.equal(response.body[0].field, 'firstname')
  assert.equal(response.body[0].validation, 'required')

  // new user data
  new_user = {
    firstname: 'john',
    email: 'cena@content-machine.com',
    password: '12345678',
    password_confirmation: '12345678'
  }

  // make api request to signup the user
  response = await client.post('api/signup').send(new_user).end()

  // assert the status is 200
  response.assertStatus(400)

  assert.equal(response.body[0].field, 'lastname')
  assert.equal(response.body[0].validation, 'required')

  // new user data
  new_user = {
    firstname: 'john',
    lastname: 'cena',
    password: '12345678',
    password_confirmation: '12345678'
  }

  // make api request to signup the user
  response = await client.post('api/signup').send(new_user).end()

  // assert the status is 200
  response.assertStatus(400)

  assert.equal(response.body[0].field, 'email')
  assert.equal(response.body[0].validation, 'required')

  // new user data
  new_user = {
    firstname: 'john',
    lastname: 'cena',
    email: 'cena@content-machine.com',
    password_confirmation: '12345678'
  }

  // make api request to signup the user
  response = await client.post('api/signup').send(new_user).end()

  // assert the status is 200
  response.assertStatus(400)

  assert.equal(response.body[0].field, 'password')
  assert.equal(response.body[0].validation, 'required')

  // new user data
  new_user = {
    firstname: 'john',
    lastname: 'cena',
    email: 'cena@content-machine.com',
    password: '12345678'
  }

  // make api request to signup the user
  response = await client.post('api/signup').send(new_user).end()

  // assert the status is 200
  response.assertStatus(400)

  assert.equal(response.body[0].field, 'password_confirmation')
  assert.equal(response.body[0].validation, 'required')
})

test('should not signUp a user successfully if the email is already used', async ({
  client,
  assert
}) => {
  // new user data
  const new_user = {
    firstname: 'john',
    lastname: 'cena',
    email: 'john@content-machine.com',
    password: '12345678',
    password_confirmation: '12345678'
  }

  // make api request to signup the user
  const response = await client.post('api/signup').send(new_user).end()

  // assert the status is 200
  response.assertStatus(400)

  assert.equal(response.body[0].field, 'email')
  assert.equal(response.body[0].validation, 'unique')
})

test('should not signUp a user successfully if the email is malformatted', async ({
  client,
  assert
}) => {
  // new user data
  const new_user = {
    firstname: 'john',
    lastname: 'cena',
    email: 'cena@content-machine.',
    password: '12345678',
    password_confirmation: '12345678'
  }

  // make api request to signup the user
  const response = await client.post('api/signup').send(new_user).end()

  // assert the status is 200
  response.assertStatus(400)

  assert.equal(response.body[0].field, 'email')
  assert.equal(response.body[0].validation, 'email')
})

test('should not signUp a user successfully if the password is less than 8 chars', async ({
  client,
  assert
}) => {
  // new user data
  const new_user = {
    firstname: 'john',
    lastname: 'cena',
    email: 'cena@content-machine.com',
    password: '1234567',
    password_confirmation: '1234567'
  }

  // make api request to signup the user
  const response = await client.post('api/signup').send(new_user).end()

  // assert the status is 200
  response.assertStatus(400)

  assert.equal(response.body[0].field, 'password')
  assert.equal(response.body[0].validation, 'min')
})

test('should not signUp a user successfully if the password_confirmation is different than password', async ({
  client,
  assert
}) => {
  // new user data
  const new_user = {
    firstname: 'john',
    lastname: 'cena',
    email: 'cena@content-machine.com',
    password: '12345678',
    password_confirmation: '12345679'
  }

  // make api request to signup the user
  const response = await client.post('api/signup').send(new_user).end()

  // assert the status is 200
  response.assertStatus(400)

  assert.equal(response.body[0].field, 'password_confirmation')
  assert.equal(response.body[0].validation, 'same')
})
