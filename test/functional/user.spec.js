'use strict'

const Factory = use('Factory')
const User = use('App/Models/User')
const { test, trait, before, after } = use('Test/Suite')('User')

const ace = require('@adonisjs/ace')

before(async () => {
  await Factory.model('App/Models/User').create({
    email: 'admin@content-machine.com',
    password: 'Admin1234',
    is_active: true,
    is_admin: true
  })
  await Factory.model('App/Models/User').create({
    email: 'user1@content-machine.com',
    password: 'Test1234',
    is_active: true
  })
  await Factory.model('App/Models/User').create({
    email: 'user2@content-machine.com',
    password: 'Test1234',
    is_active: true
  })
})

after(async () => {
  await ace.call('migration:refresh', {}, { silent: true })
})

trait('Test/ApiClient')
trait('Auth/Client')

test('should return an error if login via non admin', async ({
  assert,
  client
}) => {
  const user1 = await User.findBy('email', 'user1@content-machine.com')

  // make api request to login the user
  const response = await client
    .get('api/admin/users')
    .loginVia(user1, 'jwt')
    .end()

  // assert the status is 403
  response.assertStatus(403)
  assert.equal(response.body.message, 'Access restricted')
})

test('should return three users if login via admin', async ({
  assert,
  client
}) => {
  const admin = await User.findBy('email', 'admin@content-machine.com')

  // make api request to login the user
  const response = await client
    .get('api/admin/users')
    .loginVia(admin, 'jwt')
    .end()

  // assert the status is 200
  response.assertStatus(200)
  assert.equal(response.body.length, 3)
})

test('should not add a user successfully if login via non admin', async ({
  assert,
  client
}) => {
  const user1 = await User.findBy('email', 'user1@content-machine.com')
  // new user data
  const user3 = {
    firstname: 'user',
    lastname: '03',
    email: 'user3@content-machine.com',
    password: '12345678',
    password_confirmation: '12345678',
    is_active: true,
    is_admin: false
  }

  // make api request to login the user
  const response = await client
    .post('api/admin/users')
    .send(user3)
    .loginVia(user1, 'jwt')
    .end()
  // assert the status is 403
  response.assertStatus(403)
  assert.equal(response.body.message, 'Access restricted')
})

test('should add a user successfully if login via admin', async ({
  assert,
  client
}) => {
  const admin = await User.findBy('email', 'admin@content-machine.com')
  // new user data
  const user3 = {
    firstname: 'user',
    lastname: '03',
    email: 'user3@content-machine.com',
    password: '12345678',
    password_confirmation: '12345678',
    is_active: true,
    is_admin: false
  }

  // make api request to login the user
  const response = await client
    .post('api/admin/users')
    .send(user3)
    .loginVia(admin, 'jwt')
    .end()

  // assert the status is 200
  response.assertStatus(200)
  delete user3.password
  delete user3.password_confirmation
  response.assertJSONSubset(user3)
})

test('should not update a user successfully if login via non admin', async ({
  assert,
  client
}) => {
  const user1 = await User.findBy('email', 'user1@content-machine.com')
  // new user data
  const user3 = {
    firstname: 'user3',
    lastname: '003',
    email: 'user03@content-machine.com',
    password: '123456789',
    password_confirmation: '123456789',
    is_active: true,
    is_admin: false
  }

  // make api request to login the user
  const response = await client
    .put('api/admin/users/4')
    .send(user3)
    .loginVia(user1, 'jwt')
    .end()
  // assert the status is 403
  response.assertStatus(403)
  assert.equal(response.body.message, 'Access restricted')
})

test('should update a user successfully if login via admin', async ({
  assert,
  client
}) => {
  const admin = await User.findBy('email', 'admin@content-machine.com')
  // new user data
  const user3 = {
    firstname: 'user3',
    lastname: '003',
    email: 'user03@content-machine.com',
    password: '123456789',
    password_confirmation: '123456789',
    is_active: true,
    is_admin: false
  }

  // make api request to login the user
  const response = await client
    .put('api/admin/users/4')
    .send(user3)
    .loginVia(admin, 'jwt')
    .end()

  // assert the status is 200
  response.assertStatus(200)
  delete user3.password
  delete user3.password_confirmation
  response.assertJSONSubset(user3)
})

test('should not delete a user successfully if login via non admin', async ({
  assert,
  client
}) => {
  const user1 = await User.findBy('email', 'user1@content-machine.com')

  // make api request to login the user
  const response = await client
    .delete('api/admin/users/4')
    .loginVia(user1, 'jwt')
    .end()
  // assert the status is 403
  response.assertStatus(403)
  assert.equal(response.body.message, 'Access restricted')
})

test('should delete a user successfully if login via admin', async ({
  assert,
  client
}) => {
  const admin = await User.findBy('email', 'admin@content-machine.com')

  // make api request to login the user
  const response = await client
    .delete('api/admin/users/4')
    .loginVia(admin, 'jwt')
    .end()

  // assert the status is 200
  response.assertStatus(200)
  response.assertJSONSubset({
    firstname: 'user3',
    lastname: '003',
    email: 'user03@content-machine.com',
    is_active: 1,
    is_admin: 0
  })
})
