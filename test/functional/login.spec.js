'use strict'

const Factory = use('Factory')
const { test, trait, before, after } = use('Test/Suite')('Login')

const ace = require('@adonisjs/ace')

before(async () => {
  await Factory.model('App/Models/User').create({
    email: 'activated@content-machine.com',
    password: 'Test1234',
    is_active: true
  })
  await Factory.model('App/Models/User').create({
    email: 'inactivated@content-machine.com',
    password: 'Test1234',
    is_active: false
  })
})

after(async () => {
  await ace.call('migration:refresh', {}, { silent: true })
})

trait('Test/ApiClient')

test('should login an activated successfully', async ({ client, assert }) => {
  // activated user data
  const activated_user = {
    email: 'activated@content-machine.com',
    password: 'Test1234'
  }

  // make api request to login the user
  const response = await client.post('api/login').send(activated_user).end()

  // assert the status is 200
  response.assertStatus(200)
  // assert the user is in the response
  assert.isDefined(response.body.token)
  assert.equal(response.body.token.type, 'bearer')
  assert.isNotEmpty(response.body.token.token)
  assert.isNotEmpty(response.body.token.refreshToken)
})

test('should not login an activated user successfully if one of the fields is not present', async ({
  client,
  assert
}) => {
  // activated user data
  let activated_user = {
    password: 'Test1234'
  }

  // make api request to login the user
  let response = await client.post('api/login').send(activated_user).end()

  // assert the status is 200
  response.assertStatus(400)

  assert.equal(response.body[0].field, 'email')
  assert.equal(response.body[0].validation, 'required')

  // activated user data
  activated_user = {
    email: 'activated@content-machine.com'
  }

  // make api request to login the user
  response = await client.post('api/login').send(activated_user).end()

  // assert the status is 200
  response.assertStatus(400)

  assert.equal(response.body[0].field, 'password')
  assert.equal(response.body[0].validation, 'required')
})

test('should not login an activated user successfully if the email is malformatted', async ({
  client,
  assert
}) => {
  // activated user data
  const activated_user = {
    email: 'activated@content-machine.',
    password: 'Test1234'
  }

  // make api request to login the user
  const response = await client.post('api/login').send(activated_user).end()

  // assert the status is 200
  response.assertStatus(400)

  assert.equal(response.body[0].field, 'email')
  assert.equal(response.body[0].validation, 'email')
})

test('should not login an activated user successfully if the password is incorrect', async ({
  client,
  assert
}) => {
  // activated user data
  const activated_user = {
    email: 'activated@content-machine.com',
    password: 'Test12345'
  }

  // make api request to login the user
  const response = await client.post('api/login').send(activated_user).end()

  // assert the status is 401
  response.assertStatus(401)

  assert.equal(response.body.message, 'Wrong credentials')
})

test('should not login an inactivated user successfully', async ({
  client,
  assert
}) => {
  // activated user data
  const inactivated_user = {
    email: 'inactivated@content-machine.com',
    password: 'Test1234'
  }

  // make api request to login the user
  const response = await client.post('api/login').send(inactivated_user).end()

  // assert the status is 403
  response.assertStatus(403)

  assert.equal(response.body.message, 'Email not confirmed')
})
