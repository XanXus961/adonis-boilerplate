FROM node:14.3.0
USER node

# Create app directory
RUN mkdir -p /home/node/app && chown -R node:node /home/node/app
WORKDIR /home/node/app


# Install app dependencies
COPY --chown=node:node package.json .
COPY --chown=node:node yarn.lock .
RUN yarn install --ignore-optional

# Bundle app source
COPY --chown=node:node . .

# Install adonis cli
RUN mkdir -p /home/node/.npm-global && chown -R node:node /home/node/.npm-global
RUN npm config set prefix "/home/node/.npm-global"
ENV PATH="/home/node/.npm-global/bin:${PATH}"
RUN npm install -g @adonisjs/cli

EXPOSE 8080
CMD [ "yarn", "start" ]