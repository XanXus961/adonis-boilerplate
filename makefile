-include .env.example .env
include .make/docker.mk
include .make/private.mk
include .make/project.mk
include .make/analyse.mk
include .make/bash.mk
include .make/logs.mk
include .make/yarn.mk
include .make/adonis.mk

.DEFAULT_GOAL := help
