![MAKA Logo](https://i.imgur.com/e37gUjE.png)


[![pipeline status](https://gitlab.com/XanXus961/adonis-boilerplate/badges/develop/pipeline.svg)](https://gitlab.com/XanXus961/adonis-boilerplate/-/commits/develop)
[![coverage report](https://gitlab.com/XanXus961/adonis-boilerplate/badges/develop/coverage.svg)](https://gitlab.com/XanXus961/adonis-boilerplate/-/commits/develop)

# Adonis REST API boilerplate

This is the boilerplate for creating an API server in AdonisJs, it comes pre-configured with.

1. Bodyparser
2. Authentication
3. CORS
4. Lucid ORM
5. Migrations and seeds
6. Users CRUD
7. Tests
8. Emailing

Requirements
------------

* [docker](https://docs.docker.com/install/)
* [docker-compose](https://docs.docker.com/compose/install/)
* make

## Install
------------

The project is installed using `make`. It will start all docker containers and install all dependencies.

```bash
make install
```

## Run

The projet is run using `make`. The api will be accessible on port `8080`.

```bash
make run
```

## Useful commands

```bash
make install                                  => Install the project
make run                                      => Run the project
make stop                                     => Stop the project
make clean                                    => Clean the project
make lint                                     => Run yarn lint
make test                                     => Run yarn test
make help                                     => Show all available commands
```

## Configuration

Le fichier .env contient toute la configuration.

| Clé env | Explication | Valeur par défaut |
| ---     | ---         | ---               |
| NODE_ENV | Environnement de l'application (development, production, staging) | development |
| HOST | Hote à utiliser | 0.0.0.0 |
| PORT | Port à utiliser | 8080 |
| APP_SECRET | Clé secrete | <generated on install> |
| APP_NAME | Nom de l'application | adonis-boilerplate |
| APP_NETWORK | Le réseau de l'application utilisé par docker | adonis.maka.net |
| DB_HOST | L'URL du serveur mongodb | adonis-mysql |
| DB_PORT | Le port du serveur Mongodb | 3306 |
| DB_CONNECTION | Le type de base de données à utiliser | mysql |
| DB_DATABASE | Le nom de la base de données sur le serveur mysql | maka |
| DB_USER | L'utilisateur du serveur mysql | maka |
| DB_PASSWORD | Le mot de passe du serveur mysql | maka |
| DB_ALLOW_EMPTY_PASSWORD | Autoriser l'utilisation de la base de données sans mot de passe | maka |
| MAIL_CONNECTION | Le protocole à utiliser dans les mails | smtp |
| MAIL_USERNAME | L'utilisateur du serveur SMTP | 558e363aa501d3 |
| MAIL_PASSWORD | Le mot de passe du serveur SMTP | 32256579104d4a |
| MAIL_FROM | L'adresse de l'éxpéditeur | no-reply@maka.net |
| SMTP_HOST | L'URL du serveur SMTP | smtp.mailtrap.io |
| SMTP_PORT | Le port du serveur SMTP | 2525 |

## CHANGELOG
Changelog se trouve [ici](CHANGELOG.md)
