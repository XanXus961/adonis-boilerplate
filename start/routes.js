'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

/**
 * Swagger jsDoc
 */
const swaggerJSDoc = use('swagger-jsdoc')
Route.get('/api-docs', async ({ request, response }) => {
  const options = {
    swaggerDefinition: {
      openapi: '3.0.1',
      info: {
        title: 'Adonis REST API', // Title (required)
        version: '1.0.0' // Version (required)
      },
      components: {
        securitySchemes: {
          bearerAuth: {
            type: 'http',
            scheme: 'bearer',
            bearerFormat: 'JWT'
          }
        }
      },
      security: [
        {
          bearerAuth: []
        }
      ]
    },
    apis: ['app/**/*.js', 'start/routes.js'] // Path to the API docs
  }
  // Initialize swagger-jsdoc -> returns validated swagger spec in json format
  return swaggerJSDoc(options)
})

const addApiPrefixToGroup = (group) => {
  group.prefix('api')
  return group
}

const addAdminPrefixToGroup = (group) => {
  group.prefix('api/admin')
  return group
}

// routes with guest middleware
addApiPrefixToGroup(
  Route.group(() => {
    // signup
    Route.post('signup', 'AuthController.signup').validator('AuthSignUp')
    // activation
    Route.get('activation', 'AuthController.activation').validator(
      'AuthActivation'
    )
    // resend activation
    Route.get(
      'resendActivationEmail',
      'AuthController.resendActivationEmail'
    ).validator('AuthResendActivationEmail')
    // login
    Route.post('login', 'AuthController.login').validator('AuthLogin')
    // refresh token
    Route.get('refreshToken', 'AuthController.refreshToken')
    // forgot password
    Route.post('forgotPassword', 'AuthController.forgotPassword').validator(
      'AuthForgotPassword'
    )
    // check reset password token
    Route.get(
      'checkResetPasswordToken',
      'AuthController.checkResetPasswordToken'
    ).validator('AuthCheckResetPasswordToken')
    // reset password
    Route.post('resetPassword', 'AuthController.resetPassword').validator(
      'AuthResetPassword'
    )
  }).middleware(['guest'])
)
// routes with auth middleware
addApiPrefixToGroup(Route.group(() => {}).middleware(['auth']))

// routes with admin middleware
addAdminPrefixToGroup(
  Route.group(() => {
    Route.resource('users', 'admin/UserController')
      .apiOnly()
      .validator(
        new Map([
          [['users.store'], ['AdminUserStore']],
          [['users.update'], ['AdminUserUpdate']],
          [['users.destroy'], ['AdminUserDestroy']]
        ])
      )
  }).middleware(['auth:jwt', 'admin'])
)
